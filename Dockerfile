FROM gcr.io/cloud-solutions-images/jenkins-k8s-slave
ENV GCLOUD_SDK_VERSION 152.0.0
RUN gcloud components update --version ${GCLOUD_SDK_VERSION}
